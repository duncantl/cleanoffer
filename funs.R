library(RCurl)
library(XML)

if(FALSE) {
con = login()
z = cleanOffer(con)
}

getPropDetails =
function(r1, r3)
{
  vals = xmlSApply(r1, function(x) XML:::trim(xmlValue(x)))[c(5, 7, 9, 11, 15, 17, 19, 23)]
  c(vals, strsplit(gsub(".*([0-9]+) Bed.*([0-9]+) Bath.*", "\\1,\\2", xmlValue(r3)), ",")[[1]])
}


readTable =
function(body)
{
    i = seq(1, by = 3, length = xmlSize(body)/3)
    vals = lapply(i, function(i) getPropDetails(body[[i]], body[[i+2]]))
    dt = as.data.frame(do.call(rbind, vals), stringsAsFactors = FALSE)
    names(dt) = c("Status", "Amount", "Address", "City", "City", "Date", "Type", "Sqft", "Bed", "Bath")


    dt[,"Amount"] = as.integer(gsub("[$,]", "", dt[,"Amount"]))
    dt$Bed = as.integer(dt$Bed)
    dt$Bath = as.integer(dt$Bath)

    dt$SoldDate = as.Date(dt$Date, "%m/%d/%Y")

    dt$Sqft = as.integer(dt$Sqft)
    dt$Amount = as.integer(gsub("[$,]", "", dt$Amount))


if(FALSE) {
u = lapply(body[i+1],
             function(x) {
               tt = getNodeSet(x, ".//a[@class='thumbnail' and ./img]")
               if(length(tt))
                  xmlGetAttr(tt[[1]], "href")
               else
                  ""
             })

w = (u != "")
u = sprintf("http://re.cleanoffer.com%s", u[w])

uu = sapply(strsplit(u, "[?&]"), function(x) 
                                   sprintf("%s?%s",  x[1], x[4]))
}


    dt
}


getOriginalPrice =
  function(u) {
          x = getURLContent(u, cookie = "JSESSIONID=378EDB89804A4B93FF55B06DFA400A45")
          gsub(".*Original Price: $([,0-9]+).*", "\\1", x)
   }  





login = function(passwd = getOption("CleanOffer", stop("need cleanoffer login and password")),
                 user = names(passwd),
                 con = getCurlHandle(followlocation = TRUE, cookiejar = ""),
                 ulogin = "https://re.cleanoffer.com/j_spring_security_check")    
{
    tt = postForm(ulogin, j_username = user,
                   j_password = passwd, style = "post", curl = con)

    con
}


cleanOffer =
function(con, u = "https://re.cleanoffer.com/search/savedSearch?searchId=251678400")
{
    txt = getURLContent(u, curl = con)
    tt = getNodeSet(doc, "//table[@id = 'results']")[[1]]
    readTable(getNodeSet(tt, ".//tbody")[[1]])
}
